package com.softtek.academy.momentum.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.softtek.academy.momentum.model.User;
import com.softtek.academy.momentum.model.UserHoursByMonth;
import com.softtek.academy.momentum.model.WorkDay;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class UserServiceTest {

	@Autowired
	private UserServiceImpl userService;

	private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@Test
	public void getHoursPerMonthTest() {

		String is = "AYAD";
		String month = "01";

		UserHoursByMonth expectedUser = new UserHoursByMonth(6, is, month, "113:31:47");

		UserHoursByMonth actualUser = userService.getHoursPerMonth(is, month);

		assertNotNull(actualUser);
		assertEquals(expectedUser.getId(), actualUser.getId());
		assertEquals(expectedUser.getHours(), actualUser.getHours());
	}

	@Test
	public void getUserByPeriodTest() {

		int id = 6;
		String is = "AYAD";
		String startDate = "2019/01/01";
		String endDate = "2019/04/30";

		int idClockIn = 39674;
		String clockIn = "2019-01-02 08:41:24";
		String clockOut = "2019-01-02 13:45:57";

		dateFormat.setTimeZone(TimeZone.getTimeZone("America/Mexico City"));
		Date dateFormattedStart = new Date();
		Date dateFormattedEnd = new Date();
		try {
			dateFormattedStart = dateFormat.parse(clockIn);
			dateFormattedEnd = dateFormat.parse(clockOut);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		List<WorkDay> workDays = new ArrayList<WorkDay>();
		WorkDay firstWorkDay = new WorkDay(idClockIn, dateFormattedStart, dateFormattedEnd);
		workDays.add(firstWorkDay);

		User expectedUser = new User(id, is, workDays);

		User actualUser = userService.getUserByPeriod(is, startDate, endDate);

		assertNotNull(actualUser);
		assertEquals(expectedUser.getId(), actualUser.getId());
		assertEquals(expectedUser.getWorkDays().get(0).getId(), actualUser.getWorkDays().get(0).getId());
	}

	@Test
	public void getUsersByMonthTest() {

		String month = "01";

		List<User> userList = userService.getUsersByMonth(month);

		assertNotNull(userList);
		assertEquals(month, userList.get(0).getWorkDays().get(0).getClockIn().toString().substring(5, 7));

	}

	@Test
	public void getUsersByPeriodTest() {

		String startDate = "2019/01/01";
		String endDate = "2019/04/30";

		String expectedStartDate = "2019-01-10";

		List<User> actualUsersList = userService.getUsersByPeriod(startDate, endDate);
		User firstUser = actualUsersList.get(0);

		assertNotNull(actualUsersList);
		assertEquals(expectedStartDate, firstUser.getWorkDays().get(0).getClockIn().toString().substring(0, 10));

	}

	@Test
	public void isIntegerTest() {

		String number = "10";
		String number2 = "5.25";
		String word = "asd";

		assertTrue(userService.isInteger(number));
		assertFalse(userService.isInteger(number2));
		assertFalse(userService.isInteger(word));
	}

	@Test
	public void isDateTest() {

		String correctDate = "2019/05/10";
		String incorrectDate = "2019-13-0";
		String number = "10";

		assertTrue(userService.isDate(correctDate));
		assertFalse(userService.isDate(incorrectDate));
		assertFalse(userService.isDate(number));
	}

	@Test
	public void periodCorrectTest() {

		String date1 = "2019/01/01";
		String date2 = "2019/01/30";

		assertTrue(userService.periodCorrect(date1, date2));
		assertFalse(userService.periodCorrect(date2, date1));
	}

}
