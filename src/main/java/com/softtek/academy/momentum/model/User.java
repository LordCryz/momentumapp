package com.softtek.academy.momentum.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class User {

	@Id
	private int id;
	private String name;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "userid")
	private List<WorkDay> workDays = new ArrayList<>();

	public User() {
	}

	public User(int id, String is, List<WorkDay> workDays) {
		this.id = id;
		this.name = is;
		this.workDays = workDays;
	}

	public User(int id, String is) {
		super();
		this.id = id;
		this.name = is;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<WorkDay> getWorkDays() {
		return workDays;
	}

	public void setWorkDays(List<WorkDay> workDays) {
		this.workDays = workDays;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof User) {
			return ((User) obj).id == id;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.id;
	}

}
