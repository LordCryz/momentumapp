package com.softtek.academy.momentum.model;

public class XlsRowItem {

	private String name;
	private String id;
	private String dateTime;
	private String clock;

	public XlsRowItem() {
	}

	public XlsRowItem(String name, String id, String dateTime, String clock) {
		super();
		this.name = name;
		this.id = id;
		this.dateTime = dateTime;
		this.clock = clock;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getClock() {
		return clock;
	}

	public void setClock(String clock) {
		this.clock = clock;
	}

}
