package com.softtek.academy.momentum.model;

public class UserHoursByMonth {

	private int id;
	private String name;
	private String month;
	private String hours;

	public UserHoursByMonth() {
	}

	public UserHoursByMonth(int id, String name, String month, String hours) {
		super();
		this.id = id;
		this.name = name;
		this.month = month;
		this.hours = hours;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getHours() {
		return hours;
	}

	public void setHours(String hours) {
		this.hours = hours;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		UserHoursByMonth user = (UserHoursByMonth) obj;
		return this.id == user.id && this.name.equals(user.getName());

	}

}
