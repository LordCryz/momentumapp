package com.softtek.academy.momentum.service;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softtek.academy.momentum.model.User;
import com.softtek.academy.momentum.model.WorkDay;
import com.softtek.academy.momentum.model.XlsRowItem;
import com.softtek.academy.momentum.repository.UserJpaRepository;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
public class XlsFilterImpl implements XlsFilter {

	private static List<XlsRowItem> xlsItems = new ArrayList<XlsRowItem>();
	public static List<User> usersList = new ArrayList<User>();
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static final String xlsPath = "src//main//resources//data//Registros Momentum.xls";

	@Autowired
	UserJpaRepository userRepository;

	@Override
	public Sheet openSheet() {
		Workbook wb = null;
		try {
			wb = WorkbookFactory.create(new File(xlsPath));

		} catch (EncryptedDocumentException | IOException e) {
			e.printStackTrace();
		}
		Sheet sheetMomentumCheck = wb.getSheetAt(0);
		return sheetMomentumCheck;
	}

	@Override
	public void readXls() {

		Sheet xlsMomentum = openSheet();
		for (Row row : xlsMomentum) {

			String nameCell = row.getCell(2).getStringCellValue();
			String idCell = row.getCell(3).getStringCellValue();
			String dateTimeCell = row.getCell(4).getStringCellValue();
			String clockCell = row.getCell(6).getStringCellValue();

			XlsRowItem newItem = new XlsRowItem(nameCell, idCell, dateTimeCell, clockCell);
			xlsItems.add(newItem);

		}
		xlsItems.remove(0);
		xlsItems.sort(Comparator.comparing(XlsRowItem::getName).thenComparing(XlsRowItem::getDateTime)
				.thenComparing(XlsRowItem::getClock));
	}

	@Override
	public void getUsers() {
		HashSet<User> usersSet = new HashSet<User>();

		for (XlsRowItem item : xlsItems) {
			int userId = Integer.parseInt(item.getId());
			User newUser = new User(userId, item.getName());
			usersSet.add(newUser);

		}

		usersList = new ArrayList<User>(usersSet);
	}

	@Override
	public Date convertDateFormat(String date) {
		dateFormat.setTimeZone(TimeZone.getTimeZone("America/Mexico City"));
		Date dateFormatted = new Date();
		try {
			dateFormatted = dateFormat.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dateFormatted;
	}

	@Override
	public long getDateDiff(Date date1, Date date2) {
		long diffInMillies = date2.getTime() - date1.getTime();
		TimeUnit timeUnit = TimeUnit.MINUTES;
		return timeUnit.convert(diffInMillies, timeUnit);
	}

	@Override
	public boolean workDayisMoreOneHour(Date startDate, Date endDate) {
		return getDateDiff(startDate, endDate) >= 60;
	}

	@Override
	public List<User> filterList() {
		if (xlsItems.isEmpty()) {
			readXls();
		}
		if (usersList.isEmpty()) {
			getUsers();
		}

		for (User uniqueUser : usersList) {

			List<XlsRowItem> currentUserItems = xlsItems.stream()
					.filter(user -> Integer.parseInt(user.getId()) == uniqueUser.getId()).collect(Collectors.toList());

			List<WorkDay> workDaysList = new ArrayList<>();

			String firstDate = currentUserItems.get(0).getDateTime();
			for (int i = 1; i < currentUserItems.size(); i++) {
				String secondDate = currentUserItems.get(i).getDateTime();
				if (!(firstDate.substring(0, 10).equals(secondDate.substring(0, 10)))) {
					secondDate = currentUserItems.get(i - 1).getDateTime();
					Date startDate = convertDateFormat(firstDate);
					Date endDate = convertDateFormat(secondDate);

					if (getDateDiff(startDate, endDate) >= 60) {
						WorkDay newWorkDay = new WorkDay(startDate, endDate);
						workDaysList.add(newWorkDay);

					}
					firstDate = currentUserItems.get(i).getDateTime();
				}
				if (i + 1 == currentUserItems.size()) {
					secondDate = currentUserItems.get(i).getDateTime();
					Date startDate = convertDateFormat(firstDate);
					Date endDate = convertDateFormat(secondDate);
					if (getDateDiff(startDate, endDate) >= 60) {
						WorkDay newWorkDay = new WorkDay(startDate, endDate);
						workDaysList.add(newWorkDay);

					}
				}
			}
			uniqueUser.setWorkDays(workDaysList);
		}
		return usersList;

	}

	@Override
	public List<User> persistValidatedData() {

		userRepository.deleteAll();
		List<User> validatedUsers = filterList();
		if (!validatedUsers.isEmpty())
			userRepository.saveAll(validatedUsers);
		return validatedUsers;
	}

}
