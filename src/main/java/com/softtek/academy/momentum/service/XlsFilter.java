package com.softtek.academy.momentum.service;

import org.apache.poi.ss.usermodel.Sheet;

import com.softtek.academy.momentum.model.User;
import com.softtek.academy.momentum.model.XlsRowItem;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public interface XlsFilter {

	static List<XlsRowItem> xlsItems = new ArrayList<XlsRowItem>();
	static List<User> usersList = new ArrayList<User>();
	static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	static final String xlsPath = "src//main//resources//data//Registros Momentum.xls";

	public Sheet openSheet();

	public void readXls();

	public void getUsers();

	public Date convertDateFormat(String date);

	public long getDateDiff(Date date1, Date date2);

	public boolean workDayisMoreOneHour(Date startDate, Date endDate);

	public List<User> filterList();

	public List<User> persistValidatedData();

}
