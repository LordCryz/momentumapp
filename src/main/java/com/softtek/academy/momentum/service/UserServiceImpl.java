package com.softtek.academy.momentum.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softtek.academy.momentum.model.User;
import com.softtek.academy.momentum.model.UserHoursByMonth;
import com.softtek.academy.momentum.model.WorkDay;
import com.softtek.academy.momentum.repository.UserJpaRepository;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserJpaRepository userRepository;

	@Override
	public UserHoursByMonth getHoursPerMonth(String is, String month) {

		User userToFind = userRepository.findByName(is);
		UserHoursByMonth userWithHours = null;
		long hoursInMIllies = 0;

		if (userToFind != null) {
			List<WorkDay> workDaysOnlyMonth = userToFind.getWorkDays().stream()
					.filter(wd -> wd.getClockIn().toString().substring(5, 7).equals(month))
					.collect(Collectors.toList());

			for (WorkDay workDay : workDaysOnlyMonth) {
				hoursInMIllies = hoursInMIllies + getDateDiffInMili(workDay.getClockIn(), workDay.getClockOut());
			}

			String totalhours = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(hoursInMIllies),
					TimeUnit.MILLISECONDS.toMinutes(hoursInMIllies)
							- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(hoursInMIllies)),
					TimeUnit.MILLISECONDS.toSeconds(hoursInMIllies)
							- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(hoursInMIllies)));

			userWithHours = new UserHoursByMonth(userToFind.getId(), userToFind.getName(), month, totalhours);
		}
		return userWithHours;
	}

	@Override
	public User getUserByPeriod(String is, String startDate, String endDate) {

		User user = userRepository.findByName(is);

		if (user != null) {
			Date startDateF;
			Date endDateF;
			try {
				startDateF = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").parse(startDate + " 00:00:00");
				endDateF = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").parse(endDate + " 23:59:59");
			} catch (ParseException e) {
				user.setWorkDays(new ArrayList<>());
				return user;
			}

			List<WorkDay> workDaysByPeriod = user.getWorkDays().stream().filter(
					wd -> wd.getClockIn().compareTo(startDateF) >= 0 && wd.getClockIn().compareTo(endDateF) <= 0)
					.collect(Collectors.toList());

			user.setWorkDays(workDaysByPeriod);
		}

		return user;
	}

	@Override
	public List<User> getUsersByMonth(String month) {

		List<User> allUsersList = userRepository.findAll();

		if (allUsersList.isEmpty())
			return allUsersList;

		for (User user : allUsersList) {
			user.getWorkDays().removeIf(wd -> !(wd.getClockIn().toString().substring(5, 7).equals(month)));
		}
		allUsersList.removeIf(u -> u.getWorkDays().isEmpty());
		return allUsersList;
	}

	@Override
	public List<User> getUsersByPeriod(String startDate, String endDate) {

		List<User> allUsersList = userRepository.findAll();

		Date startDateF;
		Date endDateF;
		try {
			startDateF = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").parse(startDate + " 00:00:00");
			endDateF = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").parse(endDate + " 23:59:59");
		} catch (ParseException e) {
			return new ArrayList<User>();
		}

		for (User user : allUsersList) {
			user.getWorkDays().removeIf(
					wd -> !(wd.getClockIn().compareTo(startDateF) >= 0 && wd.getClockIn().compareTo(endDateF) <= 0));
		}
		allUsersList.removeIf(u -> u.getWorkDays().isEmpty());
		return allUsersList;
	}

	@Override
	public boolean isInteger(String str) {
		if (str == null) {
			return false;
		}
		int length = str.length();
		if (length == 0) {
			return false;
		}
		int i = 0;
		if (str.charAt(0) == '-') {
			if (length == 1) {
				return false;
			}
			i = 1;
		}
		for (; i < length; i++) {
			char c = str.charAt(i);
			if (c < '0' || c > '9') {
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean isDate(String str) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		dateFormat.setLenient(false);
		try {
			dateFormat.parse(str.trim());
		} catch (ParseException pe) {
			return false;
		}
		return true;
	}

	@Override
	public boolean periodCorrect(String date1, String date2) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		try {
			Date start = dateFormat.parse(date1);
			Date end = dateFormat.parse(date2);
			if (getDateDiffInMili(start, end) >= 0) {
				return true;
			}
		} catch (ParseException e) {
			return false;
		}
		return false;
	}

	public long getDateDiffInMili(Date date1, Date date2) {
		long diffInMillies = date2.getTime() - date1.getTime();
		TimeUnit timeUnit = TimeUnit.MILLISECONDS;
		return timeUnit.convert(diffInMillies, timeUnit);
	}
}
