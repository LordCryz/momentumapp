package com.softtek.academy.momentum.service;

import java.util.List;

import com.softtek.academy.momentum.model.User;
import com.softtek.academy.momentum.model.UserHoursByMonth;

public interface UserService {

	UserHoursByMonth getHoursPerMonth(String is, String month);

	User getUserByPeriod(String is, String startDate, String endDate);

	List<User> getUsersByMonth(String month);

	List<User> getUsersByPeriod(String startDate, String endDate);

	boolean isInteger(String str);

	boolean isDate(String str);

	boolean periodCorrect(String date1, String date2);
}
