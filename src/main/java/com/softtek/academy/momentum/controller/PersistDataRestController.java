package com.softtek.academy.momentum.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.softtek.academy.momentum.model.User;
import com.softtek.academy.momentum.service.XlsFilter;

@RestController
@RequestMapping("api/v1")
public class PersistDataRestController {

	@Autowired
	XlsFilter filterProcess;

	@RequestMapping(value = "init/persist", method = RequestMethod.GET)
	public List<User> Init() {
		List<User> usersList = filterProcess.persistValidatedData();
		return usersList;

	}

}
