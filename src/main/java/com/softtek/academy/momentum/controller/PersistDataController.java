package com.softtek.academy.momentum.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.softtek.academy.momentum.model.User;
import com.softtek.academy.momentum.model.UserHoursByMonth;
import com.softtek.academy.momentum.model.XlsRowItem;
import com.softtek.academy.momentum.service.UserService;
import com.softtek.academy.momentum.service.XlsFilter;

@Controller
public class PersistDataController {

	@Autowired
	XlsFilter filterProcess;

	@GetMapping("persistDataController")
	public String getUserByPeriod(Model model, String is, String month) {
		List<User> allUsers = filterProcess.persistValidatedData();
		model.addAttribute("allUsers", allUsers);
		return "views/initAppView";

	}

}
