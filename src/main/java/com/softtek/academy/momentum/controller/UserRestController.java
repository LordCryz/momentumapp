package com.softtek.academy.momentum.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.softtek.academy.momentum.model.ErrorJson;
import com.softtek.academy.momentum.model.User;
import com.softtek.academy.momentum.model.UserHoursByMonth;
import com.softtek.academy.momentum.service.UserService;

@RestController
@RequestMapping("api/v1")
public class UserRestController {

	@Autowired
	UserService userService;

	@RequestMapping(value = "users/{is}/{month}", method = RequestMethod.GET)
	public ResponseEntity<?> getUsersByPeriod(@PathVariable String is, @PathVariable String month) {
		if (userService.isInteger(month)) {
			UserHoursByMonth userWithHours = userService.getHoursPerMonth(is, month);
			if (userWithHours != null) {
				return new ResponseEntity<UserHoursByMonth>(userWithHours, HttpStatus.OK);
			}
		} else {
			ErrorJson error = new ErrorJson(HttpStatus.BAD_REQUEST, "Incorrect month");
			return new ResponseEntity<ErrorJson>(error, HttpStatus.BAD_REQUEST);
		}
		ErrorJson error = new ErrorJson(HttpStatus.NOT_FOUND, is + " not found");
		return new ResponseEntity<ErrorJson>(error, HttpStatus.NOT_FOUND);
	}

	@RequestMapping(value = "users", method = RequestMethod.POST)
	public ResponseEntity<?> getUser(String is, String startDate, String endDate) {
		if (userService.isDate(startDate) && userService.isDate(endDate)) {
			if (userService.periodCorrect(startDate, endDate)) {
				User user = userService.getUserByPeriod(is, startDate, endDate);
				if (user != null) {
					if (!user.getWorkDays().isEmpty()) {

						return new ResponseEntity<User>(user, HttpStatus.OK);
					} else {
						ErrorJson error = new ErrorJson(HttpStatus.NOT_FOUND, is + " work days not found");
						return new ResponseEntity<ErrorJson>(error, HttpStatus.NOT_FOUND);
					}

				} else {

					ErrorJson error = new ErrorJson(HttpStatus.NOT_FOUND, is + " not found");
					return new ResponseEntity<ErrorJson>(error, HttpStatus.NOT_FOUND);
				}
			} else {
				ErrorJson error = new ErrorJson(HttpStatus.BAD_REQUEST, "Incorrect period");
				return new ResponseEntity<ErrorJson>(error, HttpStatus.BAD_REQUEST);
			}

		} else {

			ErrorJson error = new ErrorJson(HttpStatus.BAD_REQUEST, "Incorrect date format (yyy/MM/dd)");
			return new ResponseEntity<ErrorJson>(error, HttpStatus.BAD_REQUEST);
		}

	}

	@RequestMapping(value = "period/{month}", method = RequestMethod.GET)
	public ResponseEntity<?> periodByMonth(@PathVariable String month) {
		if (userService.isInteger(month)) {
			List<User> userList = userService.getUsersByMonth(month);
			if (!userList.isEmpty())
				return new ResponseEntity<List<User>>(userList, HttpStatus.OK);

		} else {
			ErrorJson error = new ErrorJson(HttpStatus.BAD_REQUEST, "Incorrect month");
			return new ResponseEntity<ErrorJson>(error, HttpStatus.BAD_REQUEST);
		}
		ErrorJson error = new ErrorJson(HttpStatus.NOT_FOUND, "Users not found");
		return new ResponseEntity<ErrorJson>(error, HttpStatus.NOT_FOUND);
	}

	@RequestMapping(value = "period", method = RequestMethod.POST)
	public ResponseEntity<?> period(String startDate, String endDate) {
		if (userService.isDate(startDate) && userService.isDate(endDate)) {
			if (userService.periodCorrect(startDate, endDate)) {
				List<User> userList = userService.getUsersByPeriod(startDate, endDate);
				if (!userList.isEmpty()) {

					return new ResponseEntity<List<User>>(userList, HttpStatus.OK);
				} else {
					ErrorJson error = new ErrorJson(HttpStatus.NOT_FOUND, "Users not found");
					return new ResponseEntity<ErrorJson>(error, HttpStatus.NOT_FOUND);
				}
			} else {
				ErrorJson error = new ErrorJson(HttpStatus.BAD_REQUEST, "Incorrect period");
				return new ResponseEntity<ErrorJson>(error, HttpStatus.BAD_REQUEST);
			}

		} else {
			ErrorJson error = new ErrorJson(HttpStatus.BAD_REQUEST, "Incorrect date format (yyy/MM/dd)");
			return new ResponseEntity<ErrorJson>(error, HttpStatus.BAD_REQUEST);
		}
	}

}
