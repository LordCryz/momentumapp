package com.softtek.academy.momentum.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.softtek.academy.momentum.model.ErrorJson;
import com.softtek.academy.momentum.model.User;
import com.softtek.academy.momentum.model.UserHoursByMonth;
import com.softtek.academy.momentum.service.UserService;

@Controller
public class UserController {

	@Autowired
	UserService userService;

	@GetMapping("userHoursController")
	public String getUserByPeriod(Model model, String is, String month) {
		UserHoursByMonth user = userService.getHoursPerMonth(is, month);
		model.addAttribute("userHours", user);
		return "views/userHoursForm";

	}

	@PostMapping("userByPeriodController")
	public String getUser(Model model, String is, String startDate, String endDate) {
		User user = userService.getUserByPeriod(is, startDate, endDate);
		model.addAttribute("user", user);
		return "views/userByPeriodForm";

	}

	@GetMapping("usersByMonthController")
	public String getUsersByMonth(Model model, String month) {
		List<User> users = userService.getUsersByMonth(month);
		model.addAttribute("users", users);
		return "views/usersByMonthForm";

	}

	@PostMapping("usersByPeriodController")
	public String getUsersByPeriod(Model model, String startDate, String endDate) {
		List<User> users = userService.getUsersByPeriod(startDate, endDate);
		model.addAttribute("users", users);
		return "views/usersByPeriodForm";

	}

}
