package com.softtek.academy.momentum.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {

	@RequestMapping(value = "/")
	public String home() {
		return "index";
	}

	@RequestMapping(value = "userHours")
	public String userHours() {
		return "views/userHoursForm";
	}

	@RequestMapping(value = "/userByPeriod")
	public String userByPeriod() {
		return "views/userByPeriodForm";
	}

	@RequestMapping(value = "/usersByMonth")
	public String usersByMonth() {
		return "views/usersByMonthForm";
	}

	@RequestMapping(value = "/usersByPeriod")
	public String usersByPeriod() {
		return "views/usersByPeriodForm";
	}

}
