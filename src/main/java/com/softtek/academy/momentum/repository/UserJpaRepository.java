package com.softtek.academy.momentum.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.softtek.academy.momentum.model.User;

public interface UserJpaRepository extends JpaRepository<User, Long> {

	public User findByName(String name);

}
